FROM centos/python-36-centos7

ADD requirements.txt /requirements.txt
RUN pip install -r /requirements.txt

USER 0
RUN yum -y install sqlite
USER 1001
