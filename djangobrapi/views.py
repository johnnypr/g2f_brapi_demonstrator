from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.middleware.csrf import _get_new_csrf_string
import base64
import json
from django.core.cache import cache
from django.utils.crypto import get_random_string


def pixel_gif(request):
    PIXEL_GIF_DATA = base64.b64decode(b"R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7")
    return HttpResponse(PIXEL_GIF_DATA, content_type='image/gif')


@csrf_exempt
def get_token(request):
    _token = _get_new_csrf_string()
    cache.set(_token, request.META.get('HTTP_REFERER'))

    resp = JsonResponse({
        'token': _token
    })
    return resp


@csrf_exempt
def token_test(request):
    post_value = request.POST.get('token')
    sess_value = cache.get(post_value)
    cache.delete(post_value)

    token_valid = request.META.get('HTTP_REFERER') == sess_value

    results = {
        'token': post_value
        , 'referer': request.META.get('HTTP_REFERER')
        , 'referer from cache': sess_value
        , 'token valid': token_valid
    }

    dev_str = json.dumps(results, indent=3)

    print("")
    print("SUMMARY:")
    print(dev_str)
    return JsonResponse(results)

@csrf_exempt
def file_upload(request):
    if request.method == 'POST':
        #form = UploadFileForm(request.POST, request.FILES)
        #if form.is_valid():


            files = request.FILES.getlist('file')
            print(files)

            if files:
                res = []

                # Prepend with a random string in order to avoid file name clashes:
                rando = get_random_string(length=5) + "_"

                for f in files:
                    handle_uploaded_file(f, prefix=rando)
                    res.append(f.content_type)

                return HttpResponse(", ".join(res))

            else:
                return HttpResponse("no file found!")


    elif request.method == 'GET':
            return render(
                request=request
                , template_name='file_upload.html'
            )


def handle_uploaded_file(f, prefix=''):


    with open(f'./uploads/{prefix}{f.name}', 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)


def test(request, test_str):
    print("WHAT IS PARAM?", test_str)

    if test_str == 'another-test':

       with open('brapi/static/test.html', 'r') as read_handle:
           resp = read_handle.read()

    else:
        resp = f'<div><p>{test_str}</p></div>'

    return HttpResponse(resp)

