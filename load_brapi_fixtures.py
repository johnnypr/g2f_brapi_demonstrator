from django.core.management import call_command
import os
import sys
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "djangobrapi.settings")
django.setup()


FIXTURE_PATHS = (
    '../../../g2f_brapi_parser_2/code/fixtures/g2f_2016_hybrid/',
    '../../../g2f_brapi_parser_2/code/fixtures/g2f_2015_hybrid/',
    '../../../g2f_brapi_parser_2/code/fixtures/g2f_2015_inbred/',
    '../../../g2f_brapi_parser_2/code/fixtures/g2f_2014_hybrid/',
    '../../../g2f_brapi_parser_2/code/fixtures/g2f_2014_inbred/',
)


#FIXTURE_PATH = '../../../g2f_brapi_parser_2/code/fixtures/g2f_2016_hybrid/'

# Fixtures need to be loaded in a certain order to avoid failure on foreign key constraints:
fixture_files = (
    'boilerplate_fixture.json',
    #'programs_fixture.json',
    'trials_fixture.json',
    'germplasm_fixture.json',
    'locations_fixture.json',
    'studies_fixture.json',
    'obs_units_fixture.json',
    'methods_fixture.json',
    'traits_fixture.json',
    'observation_variables_fixture.json',
    'scales_fixture.json',
    'observations_fixture.json',
    'season_fixture.json'
)

#for dir, subdir, files in os.walk(FIXTURE_PATH):

for path in FIXTURE_PATHS:


    for f in fixture_files:
        print(path, f)
        call_command('loaddata', path + f)



